// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "MyPuzzleBlock3.h"
#include "PointConnect.h"
#include "Components/LineBatchComponent.h"
#include "StickCode.h"
#include "PuzzleBlockGrid.generated.h"


UCLASS()
class GOMOKU_API APuzzleBlockGrid : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APuzzleBlockGrid();

	/** Number of blocks along each side of grid */
	UPROPERTY(Category = Grid, EditAnywhere, BlueprintReadOnly)
		int32 Size;

	/** Spacing of blocks */
	UPROPERTY(Category = Grid, EditAnywhere, BlueprintReadOnly)
		float BlockSpacing;

	/** Spacing of blocks */
	UPROPERTY(Category = Grid, EditAnywhere, BlueprintReadOnly)
		bool IsX;
	UPROPERTY(Category = Grid, EditAnywhere, BlueprintReadOnly)
		int32 matchSize;

	UPROPERTY(Category = Player, EditAnywhere, BlueprintReadOnly)
		int32 Player1Score;
	UPROPERTY(Category = Player, EditAnywhere, BlueprintReadOnly)
		int32 Player2Score;
	
	UFUNCTION(BlueprintCallable, Category = "Function")
		bool CheckIfWinFunction(int32 value);

	UFUNCTION(BlueprintCallable, Category = "Setter")
		void SetSize(int32 value);
	UFUNCTION(BlueprintCallable, Category = "Setter")
		void SetBlockSpacing(float value);
	UFUNCTION(BlueprintCallable, Category = "Setter")
		void SetIsX(bool value);
	UFUNCTION(BlueprintCallable, Category = "Setter")
		void SetmatchSize(int32 value);
	UFUNCTION(BlueprintCallable, Category = "Setter")
		void SetPlayer1Score(int32 value);
	UFUNCTION(BlueprintCallable, Category = "Setter")
		void SetPlayer2Score(int32 value);

	UFUNCTION(BlueprintCallable, Category = "PlayerFunction")
		void IncreasePlayer1Score(int32 value);

	UFUNCTION(BlueprintCallable, Category = "PlayerFunction")
		void IncreasePlayer2Score(int32 value);

	UPROPERTY(Category = Grid, EditAnywhere, BlueprintReadOnly)
		TArray<AMyPuzzleBlock3*> BlockArray;

	UFUNCTION(BlueprintCallable, Category = Grid)
		void AddPuzzleBlock(AMyPuzzleBlock3* value);

	UFUNCTION(BlueprintCallable, Category = Grid)
		AMyPuzzleBlock3* GetPuzzleBlock(int32 x, int32 y);

	UFUNCTION(BlueprintCallable, Category = StickConnect)
		void CheckAndDrawLines();

	UPROPERTY(Category = StickConnect, EditAnywhere, BlueprintReadOnly)
		TArray<AStickCode*> StickCodeArray;

	UFUNCTION(BlueprintCallable, Category = StickConnect)
		void ClearStickCodeArray();

	TArray<PointConnect> ListDrawLines;
	bool CheckIsPointInListDrawLines(PointConnect PointConnectCheck);
	void DrawLine(PointConnect PointConnectCheck);

	UPROPERTY(Category = Player, EditAnywhere, BlueprintReadOnly)
		int32 aaaa;

	UFUNCTION(BlueprintCallable, Category = Grid)
		void SpawnBlocks();

	UPROPERTY(Category = Grid, EditAnywhere, BlueprintReadOnly)
		TSubclassOf<AActor> test;


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
};

