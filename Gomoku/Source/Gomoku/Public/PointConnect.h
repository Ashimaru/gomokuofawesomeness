// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

/**
 * 
 */
class GOMOKU_API PointConnect
{
private:
	int numberPointOne;
	int numberPointScecond;

public:
	PointConnect();
	PointConnect(int newNumberPointOne, int newNumberPointScecond);

	int GetNumberPointOne();
	int GetNumberPointScecond();
	~PointConnect();

	bool operator==(PointConnect right);


};