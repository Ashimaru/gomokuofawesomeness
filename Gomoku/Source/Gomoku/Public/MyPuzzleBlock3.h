// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "MyPuzzleBlock3.generated.h"

UCLASS()
class GOMOKU_API AMyPuzzleBlock3 : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMyPuzzleBlock3();

	UPROPERTY(Category = Player, EditAnywhere, BlueprintReadOnly)
		int32 BlockState;

	UFUNCTION(BlueprintCallable, Category = "Setter")
		void SetBlockState(int32 value);

	UPROPERTY(Category = Grid, EditAnywhere, BlueprintReadOnly)
		int32 x;
	UPROPERTY(Category = Grid, EditAnywhere, BlueprintReadOnly)
		int32 y;

	UFUNCTION(BlueprintCallable, Category = "Setter")
		void SetX(int32 value);

	UFUNCTION(BlueprintCallable, Category = "Setter")
		void SetY(int32 value);

	UFUNCTION(BlueprintCallable, Category = "Setter")
		void SetIsActive(bool value);

	/** Are we currently active? */
	UPROPERTY(Category = Grid, EditAnywhere, BlueprintReadOnly)
		bool IsActive;

	UFUNCTION(BlueprintCallable)
		int32 ReturnIndexElementInArray();

	/** Grid that owns us */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class APuzzleBlockGrid* OwningGrid;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;



	UFUNCTION(BlueprintCallable, Category = "PlayerFunction")
		AMyPuzzleBlock3* SetXY(int32 newX, int32 newY, int32 newBlockState);


public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	
	
};
