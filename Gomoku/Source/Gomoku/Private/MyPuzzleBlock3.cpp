// Fill out your copyright notice in the Description page of Project Settings.

#include "MyPuzzleBlock3.h"
#include "PuzzleBlockGrid.h"

// Sets default values
AMyPuzzleBlock3::AMyPuzzleBlock3()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

void AMyPuzzleBlock3::SetBlockState(int32 value)
{
	BlockState = value;
}


void AMyPuzzleBlock3::SetX(int32 value)
{
	x = value;
}

void AMyPuzzleBlock3::SetY(int32 value)
{
	y = value;
}

void AMyPuzzleBlock3::SetIsActive(bool value)
{
	IsActive = value;
}

int32 AMyPuzzleBlock3::ReturnIndexElementInArray()
{
	return x * OwningGrid->Size + y;
}


// Called when the game starts or when spawned
void AMyPuzzleBlock3::BeginPlay()
{
	Super::BeginPlay();
	BlockState = 0;
	IsActive = false;
}

AMyPuzzleBlock3 * AMyPuzzleBlock3::SetXY(int32 newX, int32 newY, int32 newBlockState)
{
	x = newX;
	y = newY;
	BlockState = newBlockState;

	return this;
}
// Called every frame
void AMyPuzzleBlock3::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

