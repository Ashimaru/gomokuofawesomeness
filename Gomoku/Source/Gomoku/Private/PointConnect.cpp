// Fill out your copyright notice in the Description page of Project Settings.

#include "PointConnect.h"

PointConnect::PointConnect()
{
}

PointConnect::PointConnect(int newNumberPointOne, int newNumberPointScecond)
{
	numberPointOne = newNumberPointOne;
	numberPointScecond = newNumberPointScecond;
}

int PointConnect::GetNumberPointOne()
{
	return numberPointOne;
}

int PointConnect::GetNumberPointScecond()
{
	return numberPointScecond;
}

PointConnect::~PointConnect()
{
}

bool PointConnect::operator==(PointConnect right)
{

	if ((numberPointOne == right.numberPointOne && numberPointScecond == right.numberPointScecond)
		|| (numberPointScecond == right.numberPointOne && numberPointOne == right.numberPointScecond))
	{
		return true;
	}

	return false;
}
