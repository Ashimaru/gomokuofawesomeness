// Fill out your copyright notice in the Description page of Project Settings.

#include "NewActorComponent.h"


// Sets default values for this component's properties
UNewActorComponent::UNewActorComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	static const FName MyProperty(TEXT("MyInt32Variable"));

	UClass* MyClass = GetClass();

	for (UProperty* Property = MyClass->PropertyLink; Property; Property = Property->PropertyLinkNext)
	{
		UIntProperty* IntProperty = Cast<UIntProperty>(Property);
		if (IntProperty && Property->GetFName() == MyProperty)
		{
			IntProperty->SetPropertyValue(Property->ContainerPtrToValuePtr<int32>(this), 123123);


			break;
		}
	}
	// ...
}


// Called when the game starts
void UNewActorComponent::BeginPlay()
{
	Super::BeginPlay();

	
	
}


// Called every frame
void UNewActorComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

