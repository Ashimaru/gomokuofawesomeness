// Fill out your copyright notice in the Description page of Project Settings.

#include "PuzzleBlockGrid.h"
#include "MyPuzzleBlock3.h"
#include <Runtime/Engine/Classes/Engine/Engine.h>


// Sets default values
APuzzleBlockGrid::APuzzleBlockGrid()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	// Set defaults

	Size = 10;
	BlockSpacing = 300;
	IsX = true;
	matchSize = 5;


	Player1Score = 0;
	Player2Score = 0;


}

bool APuzzleBlockGrid::CheckIfWinFunction(int32 value)
{

	//poziom
	for (int x = 0; x < Size; x++)
	{
		for (int y = 0; y <= Size - matchSize; y++)
		{
			bool match = true;
			for (int i = 0; i < matchSize; i++)
			{
				if (value != GetPuzzleBlock(x, y + i)->BlockState)
				{
					match = false;
				}
			}
			if (match)
				return true;
		}
	}

	//pion
	for (int x = 0; x <= Size - matchSize; x++)
	{
		for (int y = 0; y < Size; y++)
		{
			bool match = true;
			for (int i = 0; i < matchSize; i++)
			{
				if (value != GetPuzzleBlock(x + i, y)->BlockState)
				{
					match = false;
				}
			}
			if (match)
				return true;
		}
	}

	//check diagonal lines from top-left to bottom-right
	for (int x = 0; x <= Size - matchSize; x++)
	{
		for (int y = 0; y <= Size - matchSize; y++)
		{
			bool match = true;
			for (int i = 0; i < matchSize; i++)
			{
				if (value != GetPuzzleBlock(x + i, y + i)->BlockState)
				{
					match = false;
				}
			}
			if (match)
				return true;
		}
	}

	//check diagoonal lines from top-right to bottom-left
	for (int x = 0; x <= Size - matchSize; x++)
	{
		for (int y = Size - 1; y >= Size - matchSize - 1; y--)
		{
			bool match = true;
			for (int i = 0; i < matchSize; i++)
			{
				if (value != GetPuzzleBlock(x + i, y - i)->BlockState)
				{
					match = false;
				}
			}
			if (match)
				return true;
		}
	}

	return false;

}

void APuzzleBlockGrid::SetSize(int32 value)
{
	Size = value;
}

void APuzzleBlockGrid::SetBlockSpacing(float value)
{
	BlockSpacing = value;
}

void APuzzleBlockGrid::SetIsX(bool value)
{
	IsX = value;
}

void APuzzleBlockGrid::SetmatchSize(int32 value)
{
	matchSize = value;
}

void APuzzleBlockGrid::SetPlayer1Score(int32 value)
{
	Player1Score = value;
}

void APuzzleBlockGrid::AddPuzzleBlock(AMyPuzzleBlock3 * value)
{
	BlockArray.Add(value);

}

AMyPuzzleBlock3 * APuzzleBlockGrid::GetPuzzleBlock(int32 x, int32 y)
{
	return BlockArray[x * Size + y];
}

void APuzzleBlockGrid::CheckAndDrawLines()
{

	PointConnect tempPointConnect = PointConnect();

	for (int x = 0; x < Size; x++)
	{
		for (int y = 0; y < Size; y++)
		{
			if (GetPuzzleBlock(x, y)->BlockState != 0)
			{

				if (x - 1 > 0 && y - 1 > 0 && GetPuzzleBlock(x, y)->BlockState == GetPuzzleBlock(x - 1, y - 1)->BlockState)
				{
					tempPointConnect = PointConnect((x - 1) * Size + (y - 1), x *  Size + y);
					if (!CheckIsPointInListDrawLines(tempPointConnect))
					{
						DrawLine(tempPointConnect);
						ListDrawLines.Add(tempPointConnect);
					}
				}

				if (y - 1 > 0 && GetPuzzleBlock(x, y)->BlockState == GetPuzzleBlock(x, y - 1)->BlockState)
				{
					tempPointConnect = PointConnect((x)* Size + (y - 1), x *  Size + y);
					if (!CheckIsPointInListDrawLines(tempPointConnect))
					{
						DrawLine(tempPointConnect);
						ListDrawLines.Add(tempPointConnect);
					}
				}

				if (x + 1 < Size && y - 1 > 0 && GetPuzzleBlock(x, y)->BlockState == GetPuzzleBlock(x + 1, y - 1)->BlockState)
				{
					tempPointConnect = PointConnect((x + 1) * Size + (y - 1), x *  Size + y);
					if (!CheckIsPointInListDrawLines(tempPointConnect))
					{
						DrawLine(tempPointConnect);
						ListDrawLines.Add(tempPointConnect);
					}
				}

				if (x - 1 > 0 && GetPuzzleBlock(x, y)->BlockState == GetPuzzleBlock(x - 1, y)->BlockState)
				{
					tempPointConnect = PointConnect((x - 1) * Size + (y), x *  Size + y);
					if (!CheckIsPointInListDrawLines(tempPointConnect))
					{
						DrawLine(tempPointConnect);
						ListDrawLines.Add(tempPointConnect);
					}
				}

				if (x + 1 < Size && GetPuzzleBlock(x, y)->BlockState == GetPuzzleBlock(x + 1, y)->BlockState)
				{
					tempPointConnect = PointConnect((x + 1) * Size + (y), x *  Size + y);
					if (!CheckIsPointInListDrawLines(tempPointConnect))
					{
						DrawLine(tempPointConnect);
						ListDrawLines.Add(tempPointConnect);
					}
				}

				if (x - 1 > 0 && y + 1 < Size && GetPuzzleBlock(x, y)->BlockState == GetPuzzleBlock(x - 1, y + 1)->BlockState)
				{
					tempPointConnect = PointConnect((x - 1) * Size + (y + 1), x *  Size + y);
					if (!CheckIsPointInListDrawLines(tempPointConnect))
					{
						DrawLine(tempPointConnect);
						ListDrawLines.Add(tempPointConnect);
					}
				}

				if (y + 1 < Size && GetPuzzleBlock(x, y)->BlockState == GetPuzzleBlock(x, y + 1)->BlockState)
				{
					tempPointConnect = PointConnect((x)* Size + (y + 1), x *  Size + y);
					if (!CheckIsPointInListDrawLines(tempPointConnect))
					{
						DrawLine(tempPointConnect);
						ListDrawLines.Add(tempPointConnect);
					}
				}

				if (x + 1 < Size && y + 1 < Size && GetPuzzleBlock(x, y)->BlockState == GetPuzzleBlock(x + 1, y + 1)->BlockState)
				{
					tempPointConnect = PointConnect((x + 1) * Size + (y + 1), x *  Size + y);
					if (!CheckIsPointInListDrawLines(tempPointConnect))
					{
						DrawLine(tempPointConnect);
						ListDrawLines.Add(tempPointConnect);
					}
				}
			}

		}
	}
	aaaa = ListDrawLines.Num();
	//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("Some variable values: x: %d"), ListDrawLines.size));
}

void APuzzleBlockGrid::ClearStickCodeArray()
{
	for (int i = 0; i < StickCodeArray.Num(); i++)
	{
		StickCodeArray[i]->Destroy();
	}
	float max = StickCodeArray.Num();

	StickCodeArray.RemoveAt(0, max);

}

bool APuzzleBlockGrid::CheckIsPointInListDrawLines(PointConnect PointConnectCheck)
{
	for (int i = 0; i < ListDrawLines.Num(); i++)
	{
		if (ListDrawLines[i] == PointConnectCheck)
		{
			return true;
		}
	}

	return false;
}

void APuzzleBlockGrid::DrawLine(PointConnect PointConnectCheck)
{
	//LineBatchComponent = CreateDefaultSubobject<ULineBatchComponent>(TEXT("LineBatcher"));

	AMyPuzzleBlock3* one = BlockArray[PointConnectCheck.GetNumberPointOne()];
	AMyPuzzleBlock3* second = BlockArray[PointConnectCheck.GetNumberPointScecond()];
	FVector LineStart = one->GetActorTransform().GetTranslation();
	FVector LineEnd = second->GetActorTransform().GetTranslation();

	FVector Translate = FVector(0, 0, 0);
	FRotator Rotation = FRotator(0, 0, 0);
	FVector Scale = FVector(0, 0, 0);
	Translate = LineStart;


	// jak obr�ci� pion czy poziom
	if (PointConnectCheck.GetNumberPointOne() / Size == PointConnectCheck.GetNumberPointScecond() / Size)
	{
		Rotation = FRotator(0, 0, 0);
		Translate = Translate - FVector(((3 / (1.0 * Size)) * BlockSpacing) / 2, 0, 0);
		Scale = FVector(3 / (1.0f * Size), 3 / (1.0f * Size), 3 / (1.0f * Size));
	}
	else if (PointConnectCheck.GetNumberPointOne() % Size == PointConnectCheck.GetNumberPointScecond() % Size)
	{
		Rotation = FRotator(0, 90, 0);
		Translate = Translate - FVector(0, ((3 / (1.0 * Size)) * BlockSpacing) / 2, 0);
		Scale = FVector(3 / (1.0f * Size), 3 / (1.0f * Size), 3 / (1.0f * Size));
	}
	else
	{
		if (PointConnectCheck.GetNumberPointOne() % Size > PointConnectCheck.GetNumberPointScecond() % Size
			&& PointConnectCheck.GetNumberPointOne() / Size > PointConnectCheck.GetNumberPointScecond() / Size)
		{
			Rotation = FRotator(0, +45, 0);
			Translate = Translate - FVector(((3 / (1.0 * Size)) * BlockSpacing) / 2, ((3 / (1.0 * Size)) * BlockSpacing) / 2, 0);
			Scale = FVector((3 * sqrt(2)) / (1.0f * Size), 3 / (1.0f * Size), 3 / (1.0f * Size));
		}
		else if (PointConnectCheck.GetNumberPointOne() % Size < PointConnectCheck.GetNumberPointScecond() % Size
			&& PointConnectCheck.GetNumberPointOne() / Size > PointConnectCheck.GetNumberPointScecond() / Size)
		{
			Rotation = FRotator(0, -45, 0);
			Translate = Translate - FVector(-((3 / (1.0 * Size)) * BlockSpacing) / 2, ((3 / (1.0 * Size)) * BlockSpacing) / 2, 0);
			Scale = FVector((3 * sqrt(2)) / (1.0f * Size), 3 / (1.0f * Size), 3 / (1.0f * Size));
		}
		else
		{
			Rotation = FRotator(0, -45, 0);
			Translate = Translate - FVector(-((3 / (1.0 * Size)) * BlockSpacing) / 2, ((3 / (1.0 * Size)) * BlockSpacing) / 2, 0);
			Scale = FVector(3 / (1.0f * Size), 3 / (1.0f * Size), 3 / (1.0f * Size));
		}

	}

	AStickCode* NewStick = GetWorld()->SpawnActor<AStickCode>(Translate, Rotation);
	//NewStick->RootComponent->GetChildComponent(0)->SetRelativeScale3D(Scale);
	//NewStick->SetActorScale3D(Scale);
	//NewStick->SetActorRelativeLocation(FVector(NewStick->GetActorTransform().GetLocation().X, NewStick->GetActorTransform().GetLocation().Y, 120));
	NewStick->ChangeMaterial(one->BlockState);
	NewStick->SetScaleBlockMesh(Scale);

	if (NewStick != nullptr)
	{
		NewStick->OwningGrid = this;
	}

	StickCodeArray.Add(NewStick);

}


void APuzzleBlockGrid::SpawnBlocks()
{

	float spaceing = 3 / (1.0f *Size) * BlockSpacing;
	FVector position = FVector();
	float scale = 3 / (1.0f * Size);
	FVector scle = FVector(scale, scale, 1.0f);
	FActorSpawnParameters SpawnInfo;

	for (int x = 0; x < Size; x++)
	{
		for (int y = 0; y < Size; y++)
		{
			position = FVector(y*spaceing, x*spaceing, 0) + this->GetActorLocation();
			AMyPuzzleBlock3* newBlock = GetWorld()->SpawnActor<AMyPuzzleBlock3>(test, position, FRotator(0, 0, 0), SpawnInfo);
			newBlock->SetActorScale3D(scle);
			if (newBlock != nullptr)
			{
				newBlock->OwningGrid = this;
			}
			newBlock->SetX(x);
			newBlock->SetY(y);
			newBlock->SetBlockState(0);

			BlockArray.Add(newBlock);

		}
	}


}

// Called when the game starts or when spawned
void APuzzleBlockGrid::BeginPlay()
{
	Super::BeginPlay();

	static const FName MyProperty(TEXT("MyInt32Variable"));

	UClass* MyClass = GetClass();

	for (UProperty* Property = MyClass->PropertyLink; Property; Property = Property->PropertyLinkNext)
	{
		UIntProperty* IntProperty = Cast<UIntProperty>(Property);
		if (IntProperty && Property->GetFName() == MyProperty)
		{
			// Need more work for arrays
			int32 MyIntValue = IntProperty->GetPropertyValue(Property->ContainerPtrToValuePtr<int32>(this));


			IntProperty->SetPropertyValue(Property->ContainerPtrToValuePtr<int32>(this), 123123);
			break;
		}
	}

}

// Called every frame
void APuzzleBlockGrid::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void APuzzleBlockGrid::SetPlayer2Score(int32 value)
{
	Player2Score = value;
}

void APuzzleBlockGrid::IncreasePlayer1Score(int32 value)
{
	Player1Score += value;
}

void APuzzleBlockGrid::IncreasePlayer2Score(int32 value)
{
	Player2Score += value;
}
